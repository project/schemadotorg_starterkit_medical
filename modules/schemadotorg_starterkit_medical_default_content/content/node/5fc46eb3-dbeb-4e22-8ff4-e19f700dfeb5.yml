_meta:
  version: '1.0'
  entity_type: node
  uuid: 5fc46eb3-dbeb-4e22-8ff4-e19f700dfeb5
  bundle: dietary_supplement
  default_langcode: en
default:
  revision_uid:
    -
      target_id: 1
  revision_log:
    -
      value: 'Created new Dietary Supplement'
  status:
    -
      value: false
  uid:
    -
      target_id: 1
  title:
    -
      value: 'Vitamin C'
  created:
    -
      value: 1715431138
  promote:
    -
      value: true
  sticky:
    -
      value: false
  revision_translation_affected:
    -
      value: true
  moderation_state:
    -
      value: draft
  metatag:
    -
      tag: meta
      attributes:
        name: title
        content: 'Vitamin C | Schema.org Blueprints Demo'
    -
      tag: meta
      attributes:
        name: description
        content: 'Vitamin C (also known as ascorbic acid and ascorbate) is a water-soluble vitamin found in citrus and other fruits, berries and vegetables. It is also a generic prescription medication and in some countries is sold as a non-prescription dietary supplement.'
    -
      tag: meta
      attributes:
        name: robots
        content: robots
    -
      tag: link
      attributes:
        rel: canonical
        href: 'https://schemadotorg.ddev.site/supplements/vitamin-c'
    -
      tag: meta
      attributes:
        property: 'og:url'
        content: 'https://schemadotorg.ddev.site/supplements/vitamin-c'
    -
      tag: meta
      attributes:
        property: 'og:title'
        content: 'Vitamin C'
    -
      tag: meta
      attributes:
        property: 'og:description'
        content: 'Vitamin C (also known as ascorbic acid and ascorbate) is a water-soluble vitamin found in citrus and other fruits, berries and vegetables. It is also a generic prescription medication and in some countries is sold as a non-prescription dietary supplement.'
    -
      tag: meta
      attributes:
        property: 'og:video:type'
        content: movie
    -
      tag: meta
      attributes:
        property: 'og:updated_time'
        content: '2024-05-16T14:31:37+0000'
    -
      tag: meta
      attributes:
        property: 'article:modified_time'
        content: '2024-05-16T14:31:37+0000'
    -
      tag: meta
      attributes:
        name: 'twitter:title'
        content: 'Vitamin C'
    -
      tag: meta
      attributes:
        name: 'twitter:description'
        content: 'Vitamin C (also known as ascorbic acid and ascorbate) is a water-soluble vitamin found in citrus and other fruits, berries and vegetables. It is also a generic prescription medication and in some countries is sold as a non-prescription dietary supplement.'
  path:
    -
      alias: /supplements/vitamin-c
      langcode: en
      pathauto: 1
  field_editorial:
    -
      entity:
        _meta:
          version: '1.0'
          entity_type: paragraph
          uuid: c40a00f5-c62d-4d1b-ac2b-4dc840de8305
          bundle: editorial
          default_langcode: en
        default:
          status:
            -
              value: true
          created:
            -
              value: 1715431157
          behavior_settings:
            -
              value: {  }
          revision_translation_affected:
            -
              value: true
  field_metatag:
    -
      value: '{"robots":"robots"}'
  schema_cm_documentation:
    -
      markup: ''
  body:
    -
      value: '<p><strong>Vitamin C</strong> (also known as <a href="https://en.wikipedia.org/wiki/Chemistry_of_ascorbic_acid">ascorbic acid</a> and <strong>ascorbate</strong>) is a water-soluble <a href="https://en.wikipedia.org/wiki/Vitamin">vitamin</a> found in <a href="https://en.wikipedia.org/wiki/Citrus">citrus</a> and other fruits, berries and vegetables. It is also a <a href="https://en.wikipedia.org/wiki/Generic_drug">generic</a> prescription medication and in some countries is sold as a non-prescription <a href="https://en.wikipedia.org/wiki/Dietary_supplement">dietary supplement</a>. As a therapy, it is used to prevent and treat <a href="https://en.wikipedia.org/wiki/Scurvy">scurvy</a>, a disease caused by <a href="https://en.wikipedia.org/wiki/Vitamin_C_deficiency">vitamin C deficiency</a>.</p><p>&nbsp;</p>'
      format: full_html
      summary: ''
  schema_active_ingredient:
    -
      value: 'Ascorbic acid'
  schema_alternate_name:
    -
      value: 'L-ascorbic acid'
    -
      value: 'Ascorbic acid'
    -
      value: Ascorbate
  schema_citation:
    -
      value: '<ol><li><a href="https://www.medicines.org.uk/emc/product/1520/smpc">"Ascorbic acid injection 500mg/5ml"</a>. <em>(emc)</em>. July 15, 2015. <a href="https://web.archive.org/web/20201014011840/https://www.medicines.org.uk/emc/product/1520/smpc">Archived</a> from the original on October 14, 2020. Retrieved October 12, 2020.</li><li><a href="https://en.wikipedia.org/wiki/Vitamin_C#cite_ref-(emc)-2018_2-0"><strong>^</strong></a> <a href="https://web.archive.org/web/20200921155221/https://www.medicines.org.uk/emc/product/9615/smpc">"Ascorbic acid 100mg tablets"</a>. <em>(emc)</em>. October 29, 2018. Archived from <a href="https://www.medicines.org.uk/emc/product/9615/smpc">the original</a> on September 21, 2020. Retrieved October 12, 2020.</li><li><a href="https://en.wikipedia.org/wiki/Vitamin_C#cite_ref-DailyMed-2020_3-0"><strong>^</strong></a> <a href="https://dailymed.nlm.nih.gov/dailymed/drugInfo.cfm?setid=388aad52-fc01-4784-9791-1dbc80c69306">"Ascor- ascorbic acid injection"</a>. <em>DailyMed</em>. October 2, 2020. <a href="https://web.archive.org/web/20201029093116/https://dailymed.nlm.nih.gov/dailymed/drugInfo.cfm?setid=388aad52-fc01-4784-9791-1dbc80c69306">Archived</a> from the original on October 29, 2020. Retrieved October 12, 2020.</li><li>^ <a href="https://en.wikipedia.org/wiki/Vitamin_C#cite_ref-NIH2021_4-0">Jump up to:<em><strong><sup>a</sup></strong></em></a> <a href="https://en.wikipedia.org/wiki/Vitamin_C#cite_ref-NIH2021_4-1"><em><strong><sup>b</sup></strong></em></a> <a href="https://en.wikipedia.org/wiki/Vitamin_C#cite_ref-NIH2021_4-2"><em><strong><sup>c</sup></strong></em></a> <a href="https://en.wikipedia.org/wiki/Vitamin_C#cite_ref-NIH2021_4-3"><em><strong><sup>d</sup></strong></em></a> <a href="https://en.wikipedia.org/wiki/Vitamin_C#cite_ref-NIH2021_4-4"><em><strong><sup>e</sup></strong></em></a> <a href="https://ods.od.nih.gov/factsheets/VitaminC-HealthProfessional/">"Vitamin C: Fact sheet for health professionals"</a>. Office of Dietary Supplements, US National Institutes of Health. March 26, 2021. <a href="https://web.archive.org/web/20170730052126/https://ods.od.nih.gov/factsheets/VitaminC-HealthProfessional/">Archived</a> from the original on July 30, 2017. Retrieved February 25, 2024.</li><li><a href="https://en.wikipedia.org/wiki/Vitamin_C#cite_ref-Chem-Spider-2020-Vitamin-C_5-0"><strong>^</strong></a> <a href="http://www.chemspider.com/Chemical-Structure.10189562.html">"Vitamin C"</a>. <em>Chem Spider</em>. Royal Society of Chemistry. <a href="https://web.archive.org/web/20200724030511/http://www.chemspider.com/Chemical-Structure.10189562.html">Archived</a> from the original on July 24, 2020. Retrieved July 25, 2020.</li></ol>'
      format: full_html
  schema_is_proprietary:
    -
      value: false
  schema_non_proprietary_name:
    -
      value: Ascor
    -
      value: Cecon
    -
      value: Cevalin
  schema_recommended_intake:
    -
      target_population: 'India National Institute of Nutrition, Hyderabad'
      dose_value: '40.00'
      dose_unit: milligram
      frequency: Daily
    -
      target_population: 'World Health Organization'
      dose_value: '45.00'
      dose_unit: milligram
      frequency: Daily
  schema_safety_consideration:
    -
      value: '<p>Oral intake as dietary supplements in excess of requirements are poorly absorbed,<a href="https://en.wikipedia.org/wiki/Vitamin_C#cite_note-NIH2021-4"><sup>[4]</sup></a> and excesses in the blood rapidly excreted in the urine, so it exhibits low acute toxicity.<a href="https://en.wikipedia.org/wiki/Vitamin_C#cite_note-lpi2018-6"><sup>[6]</sup></a> More than two to three grams, consumed orally, may cause nausea, abdominal cramps and diarrhea. These effects are attributed to the osmotic effect of unabsorbed vitamin C passing through the intestine.<a href="https://en.wikipedia.org/wiki/Vitamin_C#cite_note-DRItext-7"><sup>[7]</sup></a><sup>: 156 </sup>In theory, high vitamin C intake may cause excessive absorption of iron. A summary of reviews of supplementation in healthy subjects did not report this problem, but left as untested the possibility that individuals with hereditary <a href="https://en.wikipedia.org/wiki/Hemochromatosis">hemochromatosis</a> might be adversely affected.<a href="https://en.wikipedia.org/wiki/Vitamin_C#cite_note-DRItext-7"><sup>[7]</sup></a><sup>: 158 </sup></p>'
      format: full_html
