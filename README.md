Table of contents
-----------------

* Introduction
* Features
* Schema.org types
* Requirements
* References
* Notes
* Issues
* Development


Introduction
------------

The **Schema.org Blueprints Starter Kit: Medical** module provides
a starting point for building out medical entities using Schema.org.


Features
--------

- Alters the Schema.org Blueprints Entity Prepopulate node links to use
  the node's about target id instead of the node's id.

- For HealthTopicContent JSON-LD, copies Schema.org properties from the
  https://schema.org/about target entity.


Schema.org types
----------------

Below are the Schema.org types created by this starter kit.

- **Place** (node:Place)  
  Entities that have a somewhat fixed, physical extension.  
  <https://schema.org/Place>

- **Physician** (node:IndividualPhysician)  
  An individual medical practitioner.  
  <https://schema.org/IndividualPhysician>

- **Health Topic** (node:HealthTopicContent)  
  <a href="https://schema.org/HealthTopicContent">HealthTopicContent</a> is <a href="https://schema.org/WebContent">WebContent</a> that is about some aspect of a health topic, e.g. a condition, its symptoms or treatments.  
  <https://schema.org/HealthTopicContent>

- **Medical Study** (node:MedicalStudy)  
  A medical study is an umbrella type covering all kinds of research studies relating to human medicine or health, including observational studies and interventional trials and registries, randomized, controlled or not.  
  <https://schema.org/MedicalStudy>

- **Medical Condition** (node:MedicalCondition)  
  Any condition of the human body that affects the normal functioning of a person, whether physically or mentally.  
  <https://schema.org/MedicalCondition>

- **Dietary Supplement** (node:DietarySupplement)  
  A product taken by mouth that contains a dietary ingredient intended to supplement the diet.  
  <https://schema.org/DietarySupplement>

- **Drug** (node:Drug)  
  A chemical or biologic substance, used as a medical therapy, that has a physiological effect on an organism.  
  <https://schema.org/Drug>


Notes
-----

- A MedicalCondition (WebPage) should be as simple as possible and then link
  to dedicated HealthTopicContent (WebPages).


Issues
------

- [Add "cause" as available property for MedicalCondition #2878](https://github.com/schemaorg/schemaorg/issues/2878)


References
----------

@see /admin/reports/schemadotorg/docs/references#MedicalEntity


Development
-----------

## [Default Content](https://www.drupal.org/project/default_content)

[Defining default content](https://www.drupal.org/docs/contributed-modules/default-content-for-d8/defining-default-content)

```bash
#  Outputs a Schema.org Medical starter kits information.    
ddev drush schemadotorg:starterkit-info schemadotorg_starterkit_medical;

# Collect default content uuids. 
echo 'node:'; ddev drush sqlq "SELECT CONCAT('- ', uuid) FROM node";

# Export default content.
ddev drush default-content:export-module schemadotorg_starterkit_medical_default_content;
```


## Installation

```
ddev install admin; ddev drush en -y schemadotorg_starterkit_medical;
ddev install admin; ddev drush en -y schemadotorg_starterkit_medical_default_content;
```
